﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickDrag : MonoBehaviour
{
    private float startPosX;
    private float startPosY;
    private float newPosX;
    private float newPosY;
    private bool held = false;
    public int bandageCount = 4;
    public int painkillerCount = 2;

    private float applyBandage = 7.5f;
    private float applyPainkillers = 1.5f;

    public Text painKillerText;
    public Text bandageText;
    public Text applyingPainKiller;
    public Text applyingBandage;


    public GameObject bandage;
    public GameObject painkiller;

    //1.11 - 1.8 x bounds for patient head
    //4.6 - 6.7 x bounds for patient left leg


     void Start()
    {
        bandage = GameObject.FindGameObjectWithTag("bandages");
        painkiller = GameObject.FindGameObjectWithTag("painkillers");
    }


    // Update is called once per frame
    void Update()
    {


      

        // What happens when the sprite is held down.
        if (held == true)
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            newPosX = mousePos.x - startPosX;
            newPosY = mousePos.y - startPosY;
            newPosX = Mathf.Clamp(newPosX, -11, 11);
            newPosY = Mathf.Clamp(newPosY, -5, 5);
            transform.localPosition = new Vector3(newPosX, newPosY, 0);
        }

        if(painkiller.transform.localPosition.x > 1.11 && painkiller.transform.localPosition.x < 1.8)
        {
            if(applyPainkillers > 0.01) //apply the painkillers to the patients head
            {
                applyPainkillers -= 0.01f;

                applyingPainKiller.text = "Applying Pain Killers" + "\n" + applyPainkillers.ToString("F2");
              
            }

           
            
        }
       // bandageText.text = "x" + bandageCount;
       // painKillerText.text = "x" + painkillerCount;
        
    }

    private void OnMouseDown()
    {
        // What happens at the very moment the sprite is clicked on.
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            startPosX = mousePos.x - transform.localPosition.x;
            startPosY = mousePos.y - transform.localPosition.y;
            held = true;
        }
    }

    private void OnMouseUp()
    {
        // What happens at the very moment the sprite is let go of.
        held = false;
    }
}
