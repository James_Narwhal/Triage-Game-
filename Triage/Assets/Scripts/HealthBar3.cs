﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar3 : MonoBehaviour
{

    private Transform bar3;

    private float totalHealth = 1f;
    public float newHealth = 1f;
    public float healthdecrease;

    // Use this for initialization
    void Start()
    {
        bar3 = transform.Find("Bar3");
        bar3.localScale = new Vector3(totalHealth, 1f);
        healthdecrease = .0015f; //can be adjusted later
    }
    
  

    public void healfunction()
    {
        if (newHealth < 1f && newHealth > 0)
        {
            newHealth += .1f;
        }


    }

    // Update is called once per frame
    void Update()
    {

        // stops health from going negative and also makes sure it is constantly depleting.
        if (newHealth > 0)
        {
            newHealth = newHealth - healthdecrease;



            bar3.localScale = new Vector3(newHealth, 1f);
        }
        //Prevents the bar from overfilling aka overheal
        if (newHealth > 1f)
        {
            newHealth = 1f;
        }

        if (healthdecrease <= 0)
        {
            healthdecrease = 0.0001f;
        }
    }
}
