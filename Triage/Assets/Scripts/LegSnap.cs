﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegSnap : MonoBehaviour
{
    private bool hand;

    private float applyTimer = 1.0f;

    public bool handOver = false;
    public GameObject change;
    public GameObject H2;
    public GameObject target;
    public GameObject Loadbar;

    public Transform applyBar;

    public AudioSource snap;
    public AudioSource scream;

    // Start is called before the first frame update
    void Start()
    {
        hand = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("e") && hand == false && handOver == true)
        {
            Loadbar.SetActive(true);
            applyTimer -= Time.deltaTime;
            applyBar.localScale = new Vector3(applyTimer, 1f);
            snap.Play();
            scream.Play();
            if (applyTimer <= 0)
            {
                hand = true;
                change.SetActive(true);
                target.SetActive(false);
                Loadbar.SetActive(false);
            }
        }
        else if (Input.GetKeyUp("e"))
        {
            applyTimer = 1.0f;
            Loadbar.SetActive(false);
        }


    }
}
