﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionGloves : MonoBehaviour
{

    public GameObject Gunshot;
    public GameObject Bone;
    public GameObject Cut;
    void Start()
    {
        Gunshot = GameObject.FindGameObjectWithTag("Gunshot(leg)");
        Bone = GameObject.FindGameObjectWithTag("BrokenLeg");
        Cut = GameObject.FindGameObjectWithTag("Cut");
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag != "Gunshot(leg)" && col.gameObject.tag != "BrokenLeg" && col.gameObject.tag != "Cut")
        {
            Physics2D.IgnoreCollision(Gunshot.GetComponent<Collider2D>(), Gunshot.GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(Cut.GetComponent<Collider2D>(), Cut.GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(Bone.GetComponent<Collider2D>(), Bone.GetComponent<Collider2D>());
        }
        if(col.gameObject.tag == "Gunshot(leg)")
        {
            Debug.Log("Collision");
            Gunshot.GetComponent<GunshotTreatment>().handOver = true;
        }
        if(col.gameObject.tag == "BrokenLeg")
        {
            Bone.GetComponent<LegSnap>().handOver = true;
        }
        if (col.gameObject.tag == "Cut")
        {
            Cut.GetComponent<LeftAmputationTreatment>().handOver = true;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag != "Gunshot(leg)" && col.gameObject.tag != "BrokenLeg" && col.gameObject.tag != "Cut")
        {
            Physics2D.IgnoreCollision(Gunshot.GetComponent<Collider2D>(), Gunshot.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "Gunshot(leg)")
        {
            Debug.Log("Collision");
            Gunshot.GetComponent<GunshotTreatment>().handOver = false;
        }
        if (col.gameObject.tag == "BrokenLeg")
        {
            Bone.GetComponent<LegSnap>().handOver = false;
        }
        if (col.gameObject.tag == "Cut")
        {
            Cut.GetComponent<LeftAmputationTreatment>().handOver = false;
        }
    }
}
