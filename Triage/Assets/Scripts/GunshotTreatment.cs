﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunshotTreatment : MonoBehaviour
{
    private bool cleaned;
    private bool hand;
    private bool bandaged;
    private GameObject thigh;
    private float applyTimer = 1.0f;

    public bool handOver = false;
    public bool alcOver = false;
    public bool bandageOver = false;
    public bool switches = false;

    public ResourceCount count;

    public GameObject H1;
    public GameObject change;
    public GameObject current;
    public GameObject global;
    public GameObject Loadbar;

    public Transform applyBar;

    public AudioSource tape;
    public AudioSource scream;
    public AudioSource AmbientPain;
    // Start is called before the first frame update
    void Start()
    {
        bandaged = false;
        hand = false;
        cleaned = false;
        H1 = GameObject.FindGameObjectWithTag("HealthBar");
        AmbientPain.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("e") && hand == false && handOver == true)
        {
            Loadbar.SetActive(true);
            applyTimer -= Time.deltaTime;
            applyBar.localScale =  new Vector3(applyTimer, 1f);
            if (applyTimer <= 0)
            {
                Debug.Log("CompleteHand");
                hand = true;
                applyTimer = 1.0f;
                Loadbar.SetActive(false);
            }
        }
        else if (Input.GetKeyUp("e"))
        {
            applyTimer = 1.0f;
            Loadbar.SetActive(false);
        }

        if (Input.GetKey("e") && cleaned == false && alcOver == true && global.GetComponent<ResourceCount>().alcCount > 0)
        {
            Loadbar.SetActive(true);
            applyTimer -= Time.deltaTime;
            applyBar.localScale = new Vector3(applyTimer, 1f);
            if (applyTimer <= 0)
            {
                scream.Play();
                global.GetComponent<ResourceCount>().alcCount -= 1;
                Debug.Log("CompleteAlc");
                cleaned = true;
                Loadbar.SetActive(false);
                applyTimer = 1.0f;
            }
        }
        else if (Input.GetKeyUp("e"))
        {
            applyTimer = 1.0f;
            Loadbar.SetActive(false);
        }

        if (Input.GetKey("e") && bandaged == false && bandageOver == true && cleaned == true && hand == true && ResourceCount.bandageCount > 0)
        {
            Loadbar.SetActive(true);
            applyTimer -= Time.deltaTime;
            applyBar.localScale = new Vector3(applyTimer, 1f);
            if (applyTimer <= 0)
            {
                tape.Play();
                ResourceCount.bandageCount -= 1;
                AmbientPain.Stop();
                current.SetActive(false);
                change.SetActive(true);
                Loadbar.SetActive(false);
                bandaged = true;
                applyTimer = 1.0f;
            }
        }
        else if (Input.GetKeyUp("e"))
        {
            applyTimer = 1.0f;
            Loadbar.SetActive(false);
        }



        if (bandaged == true && hand == true && cleaned == true && switches == false)
        {
            Debug.Log("CompleteP1");
            H1.GetComponent<HealthBar>().done = true;
            H1.GetComponent<HealthBar>().treated = true;
            switches = true;
            //set Treated == true;
            //set Done == true;
        }
    }

}
