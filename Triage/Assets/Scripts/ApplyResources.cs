﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ApplyResources : MonoBehaviour
{

    public Text applyResources;

    public float bandageTime = 5.0f;
    public float pkTime = 1.5f;



   

    // Update is called once per frame
    void Update()
    {
        if (bandageTime > 0)
        {


            bandageTime -= 0.01f;

            applyResources.text = "Applying Bandages\n" + bandageTime.ToString("F2");
        }
        if(bandageTime <= 0)
        {
            applyResources.text = "";
        }

    }
}
