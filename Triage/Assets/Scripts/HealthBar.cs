﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Transform bar;

    public float totalHealth;
    public float newHealth;
    private float barHealth;
    public bool treated = false;
    public bool done = false;

    public AudioSource dead;
    public AudioSource lowhealth;
    bool gameover = false;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Count", 0.0f, 1.0f);
        bar = transform.Find("Bar");
        bar.localScale = new Vector3(totalHealth, 1f);
        dead = GetComponent<AudioSource>();

        //healthdecrease = .001f;

    }
    /* public void healfunction()
     {
         if (newHealth < 1f && newHealth > 0)
         {
             newHealth += .1f;
         }


     }
     */

    void Count()
    {
        if(done == false)
        {
            newHealth = newHealth - 1.0f;
            barHealth = newHealth / totalHealth;
            bar.localScale = new Vector3(newHealth / totalHealth, 1f);
            if(newHealth < 10f)
            {
                lowhealth.Play();
            }
        }
        else
        {
            CancelInvoke("Count");
        }

    }

    public void SetColor(Color color)
    {
        var fillbar = bar.Find("BarFill").GetComponent<SpriteRenderer>().color = color;
    }
    // Update is called once per frame
    void Update()
    {
        Color orange = new Color(.9f, 0.5f, 0.0f);
        Color yellowgreen = new Color(.3f, 0.9f, 0.0f);

        barHealth = newHealth / totalHealth;


        //Checks if the health decrease module is negative so that the player can't be stabilizing the patient to the point where it heals them.
        /*if (healthdecrease <= 0)
        {
            healthdecrease = 0.0001f;
        }*/

        //checks if dead and plays sound
        /*if (newHealth <= 0 && gameover == false)
        {
            dead.Play();
            gameover = true;
            Invoke("switchGameOver", 5f);
        }*/
        
        //changes health color based on value
        if (barHealth < .2f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = Color.red;
        }
        else if (barHealth > .4f && barHealth < .6f)
        {
            
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        else if (barHealth > .2f && barHealth < .4f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = orange;
        }
        else if (barHealth > .6f && barHealth < .8f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = yellowgreen;
        }
        else if (barHealth > .8f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = Color.green;
        }

        if (newHealth <= 0 && done == false)
        {
            done = true;
        }
    }

    void switchGameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
