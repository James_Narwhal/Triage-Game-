﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class HealthBar2 : MonoBehaviour
{

    private Transform bar2;

    public float totalHealth;
    public float newHealth;
    private float barHealth;
    public bool treated = false;
    public bool done = false;

    public bool arm = false;
    public bool leg = false;
    public GameObject H1;
    bool gameover = false;

    public AudioSource warning;
    private bool warned = false;
    // Start is called before the first frame update
    void Start()
    {
        bar2 = transform.Find("bar2");
        bar2.localScale = new Vector3(totalHealth, 1f);
        H1 = GameObject.FindGameObjectWithTag("HealthBar");


    }
    public void healfunction()
    {
        if (newHealth < 1f && newHealth > 0)
        {
            newHealth += .1f;
        }


    }

    public void SetColor(Color color)
    {
        var fillbar2 = bar2.Find("barFill2").GetComponent<SpriteRenderer>().color = color;
    }
    // Update is called once per frame
    void Update()
    {
        Color orange = new Color(.9f, 0.5f, 0.0f);
        Color yellowgreen = new Color(.3f, 0.9f, 0.0f);
        barHealth = newHealth / totalHealth;

        // stops health from going negative and also makes sure it is constantly depleting.
        if (newHealth > 0 && done == false)
        {
            newHealth -= Time.deltaTime;
            barHealth = newHealth / totalHealth;
            bar2.localScale = new Vector3(barHealth, 1f);
        }
        //Prevents the bar2 from overfilling aka overheal
        if (newHealth > totalHealth)
        {
            newHealth = totalHealth;
        }

        
        //Checks if the health decrease module is negative so that the player can't be stabilizing the patient to the point where it heals them.
        /*if (healthdecrease <= 0)
        {
            healthdecrease = 0.0001f;
        }*/

        //checks if dead and plays sound
        //checks if dead and plays sound
        /*if (newHealth <= 0 && gameover == false)
        {
            dead.Play();
            gameover = true;
            Invoke("switchGameOver", 5f);
        }*/
        if(newHealth < 20f && warned == false)
        {
            warning.Play();
            warned = true;
        }
        //changes health color based on value
        if (barHealth < .2f)
        {
            bar2.Find("barFill2").GetComponent<SpriteRenderer>().color = Color.red;
        }
        else if (barHealth > .4f && barHealth < .6f)
        {
            bar2.Find("barFill2").GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        else if (barHealth > .2f && barHealth < .4f)
        {
            bar2.Find("barFill2").GetComponent<SpriteRenderer>().color = orange;
        }
        else if (barHealth > .6f && barHealth < .8f)
        {
            bar2.Find("barFill2").GetComponent<SpriteRenderer>().color = yellowgreen;
        }
        else if (barHealth > .8f)
        {
            bar2.Find("barFill2").GetComponent<SpriteRenderer>().color = Color.green;
        }

        if(newHealth <= 0 && done == false)
        {
            done = true;
        }
        if(leg == true && arm == true)
        {
            Debug.Log("CompleteP2");
            done = true;
        }
        if(H1.GetComponent<HealthBar>().done == true && done == true)
        {
            Debug.Log("CompleteDay");
            Invoke("switchGameOver", 5f);
        }
    }

    void switchGameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
