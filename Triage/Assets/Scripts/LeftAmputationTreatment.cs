﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftAmputationTreatment : MonoBehaviour
{
    private bool cleaned;
    private bool hand;
    private bool bandaged;
    private float applyTimer = 1.0f;

    public ResourceCount count;

    public bool handOver = false;
    public bool alcOver = false;
    public bool bandageOver = false;

    public GameObject change;
    public GameObject H2;
    public GameObject target;
    public GameObject global;
    public GameObject Loadbar;

    public Transform applyBar;

    public AudioSource tape;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("e") && hand == false && handOver == true)
        {
            Loadbar.SetActive(true);
            applyTimer -= Time.deltaTime;
            applyBar.localScale = new Vector3(applyTimer, 1f);
            if (applyTimer <= 0)
            {
                Debug.Log("CompleteHand");
                hand = true;
                applyTimer = 1.0f;
                Loadbar.SetActive(false);
            }
        }
        else if (Input.GetKeyUp("e"))
        {
            applyTimer = 1.0f;
            Loadbar.SetActive(false);
        }

        if (Input.GetKey("e") && cleaned == false && alcOver == true && global.GetComponent<ResourceCount>().alcCount > 0)
        {
            Loadbar.SetActive(true);
            applyTimer -= Time.deltaTime;
            applyBar.localScale = new Vector3(applyTimer, 1f);
            if (applyTimer <= 0)
            {
                Debug.Log("CompleteAlc");
                global.GetComponent<ResourceCount>().alcCount -= 1;
                cleaned = true;
                applyTimer = 1.0f;
                Loadbar.SetActive(false);
            }
        }
        else if (Input.GetKeyUp("e"))
        {
            applyTimer = 1.0f;
            Loadbar.SetActive(false);
        }

        if (Input.GetKey("e") && bandaged == false && bandageOver == true && ResourceCount.bandageCount > 0)
        {
            Loadbar.SetActive(true);
            applyTimer -= Time.deltaTime;
            applyBar.localScale = new Vector3(applyTimer, 1f);
            if (applyTimer <= 0)
            {
                tape.Play();
                ResourceCount.bandageCount -= 1;
                Debug.Log("CompleteBandage");
                target.SetActive(false);
                change.SetActive(true);
                bandaged = true;
                applyTimer = 1.0f;
                Loadbar.SetActive(false);
            }
        }
        else if (Input.GetKeyUp("e"))
        {
            applyTimer = 1.0f;
            Loadbar.SetActive(false);
        }

        if (bandaged == true && hand == true && cleaned == true)
        {
            H2.GetComponent<HealthBar2>().arm = true;
            //set Treated == true;
            //set Done == true;
        }
    }
}
