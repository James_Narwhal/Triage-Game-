﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenBook : MonoBehaviour
{
    private Handbook handbook;
    public GameObject leftButton;
    public GameObject rightButton;
    private Vector3 camerapos;
    SpriteRenderer rendHandbook;
    bool buttonVisible = false;

    // Start is called before the first frame update
    void Start()
    {
        
        handbook = GameObject.FindObjectOfType<Handbook>();
        //leftButton = GameObject.Find("LeftButton");
        //rightButton = GameObject.Find("RightButton");
        rendHandbook = handbook.gameObject.GetComponent<SpriteRenderer>();
    }

    public void OnMouseDown()
    {
        handbook.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * .5f, Screen.height * .25f, 3.0f));
        rendHandbook.enabled = !rendHandbook.enabled;
        buttonVisible = !buttonVisible;
        leftButton.gameObject.SetActive(buttonVisible);
        rightButton.gameObject.SetActive(buttonVisible);
    }
}