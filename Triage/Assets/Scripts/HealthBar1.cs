﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class HealthBar1 : MonoBehaviour {

    private Transform bar;
    
    private float totalHealth = 1f;
    public float newHealth = 1f;
    public float healthdecrease;

    // Use this for initialization
    void Start () {
        bar = transform.Find("Bar");
        bar.localScale = new Vector3(totalHealth, 1f);

        healthdecrease = .001f;
    }



    public void healfunction()
    {
        if (newHealth < 1f && newHealth > 0)
        {
            newHealth += .1f;
        }


    }

    // Update is called once per frame
    void Update () {

        Color orange = new Color(.9f, 0.5f, 0.0f);
        Color yellowgreen = new Color(.3f, 0.9f, 0.0f);


        // stops health from going negative and also makes sure it is constantly depleting.
        if (newHealth > 0)
        {
            newHealth = newHealth - healthdecrease;



            bar.localScale = new Vector3(newHealth, 1f);
        }
        //Prevents the bar from overfilling aka overheal
        if (newHealth > 1f)
        {
            newHealth = 1f;
        }


        //Checks if the health decrease module is negative so that the player can't be stabilizing the patient to the point where it heals them.
        if (healthdecrease <= 0)
        {
            healthdecrease = 0.0001f;
        }

        //checks if dead and plays sound
        /*(if (newHealth <= 0 && gameover == false)
        {
            dead.Play();
            gameover = true;
            Invoke("switchGameOver", 10f);
        }*/

        //changes health color based on value
        if (newHealth < .2f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = Color.red;
        }
        else if (newHealth > .4f && newHealth < .6f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        else if (newHealth > .2f && newHealth < .4f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = orange;
        }
        else if (newHealth > .6f && newHealth < .8f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = yellowgreen;
        }
        else if (newHealth > .8f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = Color.green;
        }
    }
}
