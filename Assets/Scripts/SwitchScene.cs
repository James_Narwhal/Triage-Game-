﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SwitchScene : MonoBehaviour
{
    public void OnButtonPress()
    {
        if (this.name == "Briefing1FinishButton")
        {
            SceneManager.LoadScene("BioSheets1");
        }
        if(this.name == "BeginTreatment")
        {
            Debug.Log("LoadPatient");
            SceneManager.LoadScene("Patient");
        }
    }
}
