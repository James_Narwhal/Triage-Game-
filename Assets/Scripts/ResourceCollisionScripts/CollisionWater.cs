﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWater : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Concussion;
    void Start()
    {
        //Concussion = GameObject.FindGameObjectWithTag("SnappedLeg");
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag != "HurtHead")
        {
            Physics2D.IgnoreCollision(Concussion.GetComponent<Collider2D>(), Concussion.GetComponent<Collider2D>());

        }
        if (col.gameObject.tag == "HurtHead")
        {
            Debug.Log("Collision");
            Concussion.GetComponent<Splint>().splintOver = true;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag != "HurtHead")
        {
            Physics2D.IgnoreCollision(Concussion.GetComponent<Collider2D>(), Concussion.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "HurtHead")
        {
            Debug.Log("Collision");
            Concussion.GetComponent<Splint>().splintOver = false;
        }

    }
}
