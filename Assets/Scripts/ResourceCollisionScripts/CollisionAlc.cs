﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionAlc : MonoBehaviour
{
    public GameObject Gunshot;
    public GameObject Cut;
    void Start()
    {
        Gunshot = GameObject.FindGameObjectWithTag("Gunshot(leg)");
        Cut = GameObject.FindGameObjectWithTag("Cut");
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag != "Gunshot(leg)" && col.gameObject.tag != "Cut")
        {
            Physics2D.IgnoreCollision(Gunshot.GetComponent<Collider2D>(), Gunshot.GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(Cut.GetComponent<Collider2D>(), Cut.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "Gunshot(leg)")
        {
            Debug.Log("Collision");
            Gunshot.GetComponent<GunshotTreatment>().alcOver = true;
        }
        if (col.gameObject.tag == "Cut")
        {
            Cut.GetComponent<LeftAmputationTreatment>().alcOver = true;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag != "Gunshot(leg)" && col.gameObject.tag != "Cut")
        {
            Physics2D.IgnoreCollision(Gunshot.GetComponent<Collider2D>(), Gunshot.GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(Cut.GetComponent<Collider2D>(), Cut.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "Gunshot(leg)")
        {
            Debug.Log("Collision");
            Gunshot.GetComponent<GunshotTreatment>().alcOver = false;
        }
        if (col.gameObject.tag == "Cut")
        {
            Cut.GetComponent<LeftAmputationTreatment>().alcOver = false;
        }
    }
}
