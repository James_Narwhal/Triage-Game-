﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResourceCount : MonoBehaviour
{
    public static int bandageCount = 10;
    public static int waterCount = 10;
    public static int alcCount = 10;
    public static int splintCount = 10;
    public static int burngelCount = 10;
    public static int threadCount = 10;
    public static int morphineCount = 10;

    

    //Status of each of the patients, true indicates that they are still alive, false indicates they are dead
    public bool Patient1Status = true;
    public bool Patient2Status = true;
    public bool Patient3Status = true;
    public bool Patient4Status = true;

    //Status of each treatment, false indicates that the patient can still be worked on, true states that they cannot because they are either completed or dead.
    private bool Patient1Treatment = false;
    private bool Patient2Treatment = false;
    private bool Patient3Treatment = false;
    private bool Patient4Treatment = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Patient1Treatment == true && Patient2Treatment == true && Patient3Treatment == true && Patient4Treatment == true)
        {
            Debug.Log("CompleteDay");
            Invoke("switchGameOver", 5f);
        }
    }
    void switchGameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
