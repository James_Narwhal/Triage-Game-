﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{

    public void Exit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
