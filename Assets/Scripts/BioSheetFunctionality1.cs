﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BioSheetFunctionality1 : MonoBehaviour
{

    public GameObject Char1;
    public GameObject Char2;
    public SpriteRenderer BioSheet1;
    public SpriteRenderer BioSheet2;

    private List<GameObject> bioSheets = new List<GameObject>();
    private int currentSheet = 0;
    // Start is called before the first frame update
    void Start()
    {
        Char1 = GameObject.Find("Character1");
        Char2 = GameObject.Find("Character2");
        BioSheet1 = Char1.GetComponent<SpriteRenderer>();
        BioSheet2 = Char2.GetComponent<SpriteRenderer>();

    }

    public void NextSheet()
    {
        currentSheet++; //increment biosheets
    }

    public void PrevSheet()
    {
        currentSheet--; //decrement biosheets
    }
    // Update is called once per frame
    void Update()
    {
        bioSheets.Add(Char1);
        bioSheets.Add(Char2);

        if(currentSheet > 1)
        {
            currentSheet = 0;
        }

        if(currentSheet < 0)
        {
            currentSheet = 1;
        }

        if(bioSheets[currentSheet] == Char1)
        {
            BioSheet1.color = new Color(1f, 1f, 1f, 1f);
            BioSheet2.color = new Color(1f, 1f, 1f, 0f);
        }
        if(bioSheets[currentSheet] == Char2)
        {
            BioSheet1.color = new Color(1f, 1f, 1f, 0f);
            BioSheet2.color = new Color(1f, 1f, 1f, 1f);
        }
       // Char2.transform.position = new Vector3(Char1.transform.position.x + 2, Char1.transform.position.y + 3, 1);
    }
}
