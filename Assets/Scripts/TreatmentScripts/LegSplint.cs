﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegSplint : MonoBehaviour
{
    private bool splinted;
    private float applyTimer = 1.0f;
    public bool splintOver = false;

    public GameObject H2;
    public GameObject change;
    public GameObject target;
    public GameObject global;
    public GameObject Loadbar;

    public Transform applyBar;

    public AudioSource tape;
    // Start is called before the first frame update
    void Start()
    {
        splinted = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("e") && splinted == false && splintOver == true && ResourceCount.splintCount > 0)
        {
            Loadbar.SetActive(true);
            applyTimer -= Time.deltaTime;
            applyBar.localScale = new Vector3(applyTimer, 1f);
            applyTimer -= Time.deltaTime;
            if (applyTimer <= 0)
            {
                Loadbar.SetActive(false);
                tape.Play();
                ResourceCount.splintCount -= 1;
                splinted = true;
                target.SetActive(false);
                change.SetActive(true);
                H2.GetComponent<HealthBar2>().leg = true;
            }
        }
        else if (Input.GetKeyUp("e"))
        {
            applyTimer = 1.0f;
            Loadbar.SetActive(false);
        }

    }
}
